#!/bin/bash

# This script resolves plugin library dependencies,
# builds a docker image out of the plugin directory,
# and uploads the image to the appropriate Azure
# Container registry, depending on the value given
# in $REGISTRY_ENVIRONMENT.

docker login reekohstg.azurecr.io -u reekohstg -p 78/g2KRF2GGrLjNHoPmcTBJobaTStpbY

REGISTRY_ENVIRONMENT=$1
NAMESPACE=$2
VERSION=$3
ERROR=0

if [ -z REGISTRY_ENVIRONMENT ]; then
  ERROR=1
fi

if [ -z $NAMESPACE ]; then 
  ERROR=1
fi

if [ -z $VERSION ]; then 
  ERROR=1
fi

if [ $ERROR -eq 1 ]; then
	printf "\nInvalid usage.\n\nUsage: \n\n${0} ENVIRONMENT NAMESPACE VERSION\n\nExample:\n\n${0} staging reekoh 1.0.0\n\n" 
        exit 1
fi

PLUGIN_NAME=${PWD##*/}

if [ $REGISTRY_ENVIRONMENT = "staging" ] ; then
  CONTAINER_PATH="reekohstg.azurecr.io/${NAMESPACE}/${PLUGIN_NAME}"
elif [ $REGISTRY_ENVIRONMENT = "production" ] ; then
  CONTAINER_PATH="reekohusw.azurecr.io/${NAMESPACE}/${PLUGIN_NAME}"
elif [ $REGISTRY_ENVIRONMENT = "qa" ] ; then
  CONTAINER_PATH="reekohqa.azurecr.io/${NAMESPACE}/${PLUGIN_NAME}"
else
  echo "Valid values for ENVIRONMENT are: staging,production"
  ERROR=2
fi

if [ ! -e ./package.json ]; then
  echo "Can't locate 'package.json' in current directory."
  ERROR=2
fi

if [ ! -e ./Dockerfile ]; then
  echo "Cant locate 'Dockerfile' in current directory."
  ERROR=2
fi

if [ $ERROR -eq 2 ]; then
	exit 2
fi
if [ -e ./node_modules ]; then
echo "Removing 'node_modules'.."
rm -r ./node_modules
fi

echo
echo "Running 'npm i --production'.."
npm i --production

if [ $? -ne 0 ]; then
echo "Error in npm dependency resolution"
fi

echo

echo "Building ${CONTAINER_PATH}:${VERSION}"
docker build -t ${CONTAINER_PATH}:${VERSION} .

if [ $? -ne 0 ]; then
	echo "Build failed."
	ERROR=3
fi

echo "Building ${CONTAINER_PATH}:latest"
docker build -t ${CONTAINER_PATH}:latest .

if [ $? -ne 0 ]; then
	echo "Build failed."
	ERROR=3
fi

if [ $ERROR -eq 3 ]; then
	exit 3
fi


echo "Pushing ${CONTAINER_PATH}:${VERSION}"
docker push ${CONTAINER_PATH}:${VERSION}

if [ $? -ne 0 ]; then
	echo "Image upload failed."
	ERROR=4
fi

echo "Pushing ${CONTAINER_PATH}:latest"
docker push ${CONTAINER_PATH}:latest

if [ $? -ne 0 ]; then
	echo "Image upload failed."
	ERROR=4
fi

if [ $ERROR -eq 4 ]; then
	exit 4
fi
