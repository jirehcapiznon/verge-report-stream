FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/verge-report-stream

WORKDIR /home/node/verge-report-stream

RUN npm i --production

EXPOSE 8080

CMD ["node", "app.js"]
