/* global describe, it, after, before */
'use strict'

const async = require('async')
const amqp = require('amqplib')
const mongoose = require('mongoose')

const BROKER = 'amqp://guest:guest@127.0.0.1/'
const Broker = require('../node_modules/reekoh/lib/broker.lib')

const DemoPlatformEngine = require('./misc/demo-platform-engine')

let _app = null
let _conn = null
let _broker = null
let _channel = null

let DeviceModel = mongoose.model('Device')
mongoose.Promise = global.Promise

describe('Verge Mongodb Stream Test', () => {
  process.env.ACCOUNT = 'demo.account'
  process.env.PLUGIN_ID = 'demo.stream'
  process.env.OUTPUT_PIPES = 'demo.outpipe1'
  process.env.COMMAND_RELAYS = 'demo.relay1'
  process.env.CONFIG = '{"scheduled": "33,20,*,*", "connString":""}'

  process.env.MONGO_HOST = 'mongodb://127.0.0.1/'
  process.env.BROKER = 'amqp://guest:guest@127.0.0.1/'

  process.env.REDIS_PORT = '6379'
  process.env.REDIS_PASSWORD = ''
  process.env.REDIS_HOST = '127.0.0.1'

  let demoPlatformEngine = new DemoPlatformEngine()

  before('init', function (done) {
    _broker = new Broker()

    amqp.connect(BROKER).then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
      _channel = channel
    }).catch((err) => {
      console.log(err)
    })

    mongoose.connect(process.env.MONGO_HOST, err => {
      if (err) return console.log(err)

      let device1 = new DeviceModel({
        name: 'Test Device',
        _id: '567827489028375',
        account: 'demo.account',
        connectionStatus: true,
        active: true
      })

      let device2 = new DeviceModel({
        name: 'Test Device',
        _id: '567827489028376',
        account: 'demo.account',
        connectionStatus: false,
        active: true
      })

      device1.save()
        .then(() => device2.save())
        .then(() => demoPlatformEngine.init())
        .then(() => done())
        .catch(console.error)
    })
  })

  after('terminate', function () {
    DeviceModel.remove({ _id: { $in: ['567827489028375', '567827489028376'] } }).exec()
    DeviceModel.remove({_id: {$in: ['567827489028375', '567827489028376']}}).exec()
    _conn.close()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(92000000000)

      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#test RPC preparation', () => {
    it('should connect to broker', (done) => {
      _broker.connect(BROKER).then(() => {
        return done() || null
      }).catch((err) => {
        done(err)
      })
    })
    // for request device info
    it('should spawn temporary RPC server(device info)', (done) => {
      _broker.createRPC('server', 'device.info.rpc').then((queue) => {
        return queue.serverConsume((msg) => {
          return new Promise((resolve, reject) => {
            async.waterfall([
              async.constant(msg.content.toString('utf8')),
              async.asyncify(JSON.parse)
            ], (err, parsed) => {
              if (err) return reject(err)
              parsed.foo = 'bar'
              resolve(JSON.stringify(parsed))
            })
          })
        })
      }).then(() => {
        // Awaiting RPC requests
        done()
      }).catch((err) => {
        done(err)
      })
    })

  })

  // describe('#data', function () {
  //   it('should process the data', (done) => {
  //     this.timeout(10000)
  //     done()
  //   })
  // })
})

