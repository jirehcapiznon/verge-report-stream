'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

var ModelSchema = new Schema({
  _id: {
    type: String,
    required: 'Please enter plugin id.'
  },
  name: {
    type: String,
    trim: true
  },
  account: {
    type: String,
    trim: true
  },
  connectionStatus: {
    type: Boolean,
    default: false
  },
  active: {
    type: Boolean,
    default: false
  }
})

mongoose.model('Device', ModelSchema, 'temp-devices')
