'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

var ModelSchema = new Schema({
  _id: {
    type: String,
    label: 'ID',
    required: 'Kindly specify an id for this command log.',
    trim: true
  },
  account: {
    type: String,
    required: 'Kindly specify to which account this command log is associated to.',
    trim: true,
    select: false,
    index: true
  },
  targetDevice: {
    type: String,
    label: 'Target Device',
    required: 'Kindly specify the target device for the command.',
    ref: `Device`,
    trim: true,
    index: true
  },
  tags: [{
    type: String,
    trim: true,
    select: false
  }],
  date: {
    type: Date,
    default: Date.now,
    expires: '14d'
  },
  command: {
    type: String,
    trim: true
  },
  response: {
    type: String,
    trim: true
  }
})

mongoose.model('CommandLog', ModelSchema, 'temp-command-logs')
