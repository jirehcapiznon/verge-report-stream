'use strict'

require('./temp-devices.model')
require('./temp-command-log.model')

const _ = require('lodash')
const Redis = require('ioredis')
const Chance = require('chance')
const BPromise = require('bluebird')
const mongoose = require('mongoose')
const Broker = require('../../node_modules/reekoh/lib/broker.lib')

// NOTE: this is a simplified PE and was intended to use inline with test.js
// mogoose.connect() must be called first before initializing this one

class DemoPlatformEngine {
  constructor () {
    this.broker = new Broker()
    this.chance = new Chance()
    this.BROKER = process.env.BROKER
    this.safeParse = BPromise.method(JSON.parse)

    this.Device = mongoose.model('Device')
    this.CommandLog = mongoose.model('CommandLog')
  }

  init () {
    return this.initCache()
      .then(() => this.broker.connect(this.BROKER))
      .then(() => this.broker.createQueue('cmd.process'))
      .then(() => this.broker.createQueue('cmd.responses'))
      .then(() => this.broker.createRPC('server', 'cmd.pending.rpc'))
      .then(() => this.broker.createRPC('server', 'device.info.rpc'))
      .then(() => this.broker.createRPC('server', 'plugin.state.rpc'))

      .then(() => this.broker.queues['cmd.process'].consume(this.cmdProcessListener.bind(this)))
      .then(() => this.broker.queues['cmd.responses'].consume(this.cmdResponsesListener.bind(this)))
      .then(() => this.broker.rpcs['device.info.rpc'].serverConsume(this.deviceInfoListener.bind(this)))
      .then(() => this.broker.rpcs['cmd.pending.rpc'].serverConsume(this.cmdPendingListener.bind(this)))
  }

  initCache () {
    return new Promise((resolve, reject) => {
      console.log('Connecting to Redis Server...')

      let redis = new Redis({
        keepAlive: 10000,
        dropBufferSupport: true,
        enableOfflineQueue: false,
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        password: process.env.REDIS_PASSWORD,
        retryStrategy: function () { return 3000 },
        reconnectOnError: function () { return true }
      })

      redis.on('error', (err) => {
        console.log(err)
        reject(err)
      })

      redis.on('close', () => {
        console.log('Redis client has disconnected.')
      })

      redis.on('end', () => {
        console.log('Redis connection has ended.')
      })

      redis.on('reconnecting', () => {
        console.log('Reconnecting to Redis Server...')
      })

      redis.on('connect', () => {
        console.log('Connected to Redis Server.')
      })

      redis.once('ready', () => {
        console.log('Redis Server ready.')

        resolve(redis)
      })

      this.cache = redis
    })
  }

  deviceInfoListener (msg) {
    return this.safeParse(msg.content.toString()).then(content => {
      content.foo = 'bar'
      return JSON.stringify(content)
    }).catch(console.error)
  }

  cmdProcessListener (msg) {
    return this.safeParse(msg.content.toString()).then(content => {
      // console.log('-pe', msg.content.toString())
      return BPromise.props({
        // simplified version, no capturing of group devices. this was tested in PE
        devices: Array.isArray(content.devices) ? content.devices : (_.isString(content.devices) ? [content.devices] : []),
        tags: [content.pluginId, 'Foo Name']
      }).then(result => {
        // console.log(result)
        return BPromise.each(result.devices, deviceId => {
          
          return this.getDeviceStatus(content.account, deviceId).then(online => {
            // console.log('d', deviceId, online)
            let commandId = this.chance.guid()

            let cmdInfoStr = {
              device: deviceId,
              commandId: commandId,
              command: content.command,
              sequenceId: content.sequenceId
            }

            let commandLog = new this.CommandLog({
              _id: commandId,
              tags: result.tags,
              targetDevice: deviceId,
              account: content.account,
              command: content.command,
              response: ''
            })

            return commandLog.save().then(() => {
              if (online) {
                // console.log('a', cmdInfoStr)
                // publish to SDK, sdk will emit the command to 3pt plugin
                this.broker.channel.publish('amq.topic', `${content.commandRelayId}.topic`, Buffer.from(JSON.stringify(cmdInfoStr)))
              } else {
                // console.log('b', cmdInfoStr)
                // device is offline saving it to redis for 12hours
                let key = `${content.commandRelayId}-${deviceId}-${content.sequenceId}`

                return this.cache.multi()
                  .hmset(key, cmdInfoStr)
                  .expire(key, 43200 /* 12h */)
                  .exec()
              }
            })
          })
        })
      })
    }).catch(console.error)
  }

  getDeviceStatus (account, deviceId) { // fetch from cache?
    return this.Device.findOne({account: account, _id: deviceId}).exec().then(doc => {
      if (!doc) console.log(`WARNING: Can't find device id '${deviceId}' in db, throwing status as 'offline'`)

      doc = doc || {}
      return !!doc.connectionStatus
    })
  }

  cmdPendingListener (msg) {
    return this.safeParse(msg.content.toString()).then(content => {
      if (!_.get(content, 'device._id')) {
        return BPromise.reject(new Error(`Device or device id not set. data: ${content}`))
      }
      if (!_.get(content, 'device.cmdRelays')) {
        return BPromise.reject(new Error(`Command relay Id for pending command request not set. data: ${content}`))
      }
      if (_.isEmpty(_.get(content, 'device.cmdRelays'))) {
        return BPromise.reject(new Error(`Command relay must not be empty. data: ${content}`))
      }

      let pipeline = this.cache.pipeline()

      return BPromise.map(content.device.cmdRelays, cmdRelay => {
        return this.cache.keys(`${cmdRelay}:${content.device._id}:*`)
      }).then(keyMap => {
        return BPromise.each(_.flattenDeep(keyMap), key => {
          return pipeline.hgetall(key).del(key)
        })
      }).then(() => {
        return pipeline.exec()
      }).then(cachedCommands => {
        return BPromise.map(cachedCommands, cache => {
          return _.isPlainObject(cache[1]) ? cache[1] : null
        })
      }).then(commands => {
        commands = commands.filter(Boolean)
        return BPromise.resolve(JSON.stringify(commands))
      })
    }).catch(console.error)
  }

  cmdResponsesListener (msg) {
    this.safeParse(msg.content.toString() || '{}').then(content => {
      if (!content.commandId) {
        return BPromise.reject(new Error(`Command Id not defined. data: ${content}`))
      }
      if (!content.response) {
        return BPromise.reject(new Error(`Command response not defined. data: ${content}`))
      }

      return this.CommandLog
        .findOneAndUpdate({_id: content.commandId}, {response: content.response})
        .exec()
    }).catch(console.error)
  }
}

module.exports = DemoPlatformEngine
